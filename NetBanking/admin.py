from django.contrib import admin
from .models import create_kyc, create_applicationform, account_details, Transactions

# Register your models here.
admin.site.register([create_applicationform, create_kyc, account_details, Transactions])
