from django.db import models
from django.contrib.auth.models import User


class create_applicationform(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    firstname = models.CharField(max_length=255)
    lastname  = models.CharField(max_length=255)  
    parentname = models.CharField(max_length=255)
    gender = models.CharField(max_length=255)
    mobilenumber = models.IntegerField()
    emailaddress = models.EmailField(max_length=255)
    aadharnumber = models.IntegerField()
    address = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    pincode = models.IntegerField()
    verified = models.BooleanField(default=False)
    token = models.CharField(max_length=100, null=True, blank=True)
    uname = models.CharField(max_length=255)


class create_kyc(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    firstname = models.CharField(max_length=255)
    lastname  = models.CharField(max_length=255)
    nominalname = models.CharField(max_length=255)
    nominalrelation  = models.CharField(max_length=255)
    nationality  = models.CharField(max_length=255)
    gender = models.CharField(max_length=255)
    maritalstatus = models.CharField(max_length=255)
    dateofbirth = models.IntegerField()
    aadharnumber = models.IntegerField()
    mobilenumber = models.IntegerField()
    emailaddress = models.CharField(max_length=255)
    residentialaddress = models.IntegerField()
    place = models.CharField(max_length=255)
    district = models.CharField(max_length=255)
    pincode = models.IntegerField()
    state = models.CharField(max_length=255)
    country = models.CharField(max_length=255)
    perminantaddress = models.IntegerField()
    signature = models.ImageField(null = True,blank=True,upload_to ='uploads/')

class account_details(models.Model):
    account_holder = models.OneToOneField(User, on_delete=models.CASCADE)
    account_number = models.IntegerField()
    account_type = models.CharField(max_length=255, default='Savings')
    balance = models.IntegerField(default=0)
    is_kyc_verified = models.BooleanField(default=False)

class Transactions(models.Model):
    account_holder = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    sender_receiver = models.CharField(max_length=255, default='')
    intial = models.PositiveIntegerField(default=0)
    final = models.PositiveIntegerField(default=0)
    amount = models.PositiveIntegerField(default=0)

class Loans(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    loan_amount = models.IntegerField()
    years = models.IntegerField()
    interest = models.IntegerField(default=15)