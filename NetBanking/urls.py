from django.urls import path
from .import views


urlpatterns = [
    path('login/', views.Login, name='login'),
    path('logout/', views.Logout, name='logout'),
    path('success/', views.success, name='success'),
    path('failed/', views.failed, name='failed'),
    path('frontpage/',views.frontpage, name = "frontpage"),
    path('acc_num/',views.account_number, name = "accountnumber"),
    path('check/',views.check_balance, name = "check_balance" ),
    path('mini_statement/',views.mini_statement, name = "mini_statement" ),
    path('transaction/',views.transaction,name ="transaction"),
    path('loan-amount/', views.loan_amount, name='loan_amount'),
    path('loan/', views.loan, name = "loan"),
    path('forgot/',views.forgetpassword, name="forgot"),
    path('changepassword/',views.changepassword, name="changepassword"),
    path("application/",views.application_detials,name='application'),
    path('kycform/',views.KYCform,name='kycform'),
    # path('send/', views.mail, name = "send"),
    path('home/', views.home, name = "home"),
    path('register/', views.RegistrationView, name = "register"),
    path('verify/<str:token>', views.verify, name = "verify"),
    path('manager/', views.ManagerPortal, name = "manager"),
    path('authorize-user/<int:id>', views.authorize_users, name = "authorize-user"),
    path('transaction/',views.transaction,name =" transaction")
]
