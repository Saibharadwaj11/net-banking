# Generated by Django 3.2.12 on 2023-03-14 05:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('NetBanking', '0004_loans'),
    ]

    operations = [
        migrations.AddField(
            model_name='account_details',
            name='is_kyc_verified',
            field=models.BooleanField(default=False),
        ),
    ]
