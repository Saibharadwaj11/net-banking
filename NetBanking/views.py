from django.shortcuts import render, redirect
from .models import create_applicationform,create_kyc, account_details, Transactions, Loans
from django.core.mail import send_mail
from django.http import HttpResponse
from django.conf import settings
from .form import RegistrationForm
from django.contrib.sites.shortcuts import get_current_site
import random
from django.core.mail import send_mail
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
import random, array


# Create your views here.
def Login(request):
    return render(request,'Login.html')
def success(request):
    return render(request,'success.html')
def failed(request):
    return render(request,'failed.html')

def Logout(request):
    logout(request)
    return redirect('login')

def forgetpassword(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        print(username)
        user = User.objects.get(username = username)
        new_password = get_random_password()
        user.set_password(new_password)
        user.save()
        subject = 'Your new password' 
        message = f'''Your new password: {new_password}'''
        email_from = settings.EMAIL_HOST_USER
        recipient_list = [user.email,]
        send_mail( subject, message, email_from, recipient_list)
        return HttpResponse('Check your mail for new password')
    return render(request,'forgetpassword.html')

def loan(request):
    try:
        request.user.create_kyc
        return render(request,'loan.html')
    except:
        return redirect('kycform')



def changepassword(request):
    if request.method == 'POST':
        password = request.POST['password']
        repassword = request.POST['repassword']
        user = request.user
        if password == repassword:
            user.set_password(password)
            user.save()
            return redirect('login')
        else:
            return HttpResponse('both passwords are not same')
    return render(request,'changepassword.html')

def frontpage(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        
        if user:
            login(request, user)
            acc_num = user.account_details.account_number
            return render(request, 'frontpage.html', {'acc_num': acc_num})
        else:
            return render('Wrong credentials')
    elif request.method == 'GET':
        if request.user.is_authenticated:
            acc_num = request.user.account_details.account_number
            return render(request, 'frontpage.html', {'acc_num': acc_num})
    return HttpResponse('failed')

def account_number():
    MAX_LEN = 15
    DIGITS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    COMBINED_LIST = DIGITS 
    rand_digit = random.choice(DIGITS)
    temp_pass = rand_digit 
    for x in range(MAX_LEN - 4):
        temp_pass = temp_pass + random.choice(COMBINED_LIST)
        temp_pass_list = array.array('u', temp_pass)
        random.shuffle(temp_pass_list)
        account_number = ""
    for x in temp_pass_list:
        account_number = account_number + x
    return account_number

 
def application_detials(request):
    if request.method == 'GET':
        return render(request,'ApplicationForm.html')
    elif request.method == 'POST':
        firstname = request.POST['firstname']
        lastname = request.POST['lastname']
        parentname = request.POST['parentname']
        gender = request.POST['gender']
        mobilenumber = request.POST['mobilenumber']
        emailaddress = request.POST['emailaddress']
        aadharnumber = request.POST('aadharnumber')
        address = request.POST['address']
        city = request.POST['city']
        pincode = request.POST['pincode']
        verified  = request.POST['verfied']
        token = request.POST['token']
        uname = request.POST['uname']



        application_details = create_applicationform(firstname = firstname, lastname = lastname, parentname = parentname, gender = gender, 
                                                     mobilenumber= mobilenumber, emailaddress = emailaddress, aadharnumber = aadharnumber, address  =address, city = city, pincode = pincode,
                                                     verified = verified, token = token, uname = uname )
        return render(request,'ApplicationForm.html',{"application_details" : application_details})
    return HttpResponse('congrats')
 
def KYCform(request):
    if request.method == 'GET':
        return render(request,'KYCform.html')
    if request.method == 'POST':
        firstname = request.POST.get('firstname')
        lastname  = request.POST.get('lastname')
        nominalname=request.POST.get('nominalname')
        nationality=request.POST.get('nationality')
        gender= request.POST.get('gender')
        maritalstatus = request.POST.get('maritalstatus')
        dateofbirth=request.POST.get('dateofbirth')
        pan = request.POST.get('pan')
        mobilenumber = request.POST.get('mobile')
        emailaddress =request.POST.get('email')
        residentialaddress =request.POST.get('residential')
        place=request.POST.get('place')
        district=request.post.get('district')
        pincode =request.POST.get('pincode')
        state= request.POST.get('state')
        city = request.POST.get('city')
        country=request.POST.get('country')
        permanentaddress=request.POST.get('permanent')
        place=request.POST.get('place')
        district=request.post.get('district')
        pincode = request.POST.get('pincode')
        state= request.POST.get('state')
        country=request.POST.get('country')
        signature=request.Post.get('filename')
        kyc_details = create_kyc(firstname=firstname,lastname=lastname ,nominalname=nominalname, nationality = nationality ,gender = gender  , maritalstatus= maritalstatus,dateofbirth= dateofbirth,pan=pan, mobile= mobilenumber ,email =  emailaddress,  residential = residentialaddress,  place=place,district = district, pincode = pincode ,state= state, city = city,country= country,permanant = permanentaddress,filename= signature )
        kyc_details.save()
        return render(request,'ApplicationForm.html',{"kyc_details" : kyc_details})


# def mail(request):
#     subject = 'Reset Password' 
#     message = '''Reset your password from Here
#     Reset password: {changepassword()}'''
#     email_from = settings.EMAIL_HOST_USER
#     recipient_list = ['saibharadwaj7242@gmail.com',]
#     send_mail( subject, message, email_from, recipient_list)
#     return HttpResponse('check your mail for reseting the password') 


def home(request):
    return render(request,'home.html')

def RegistrationView(request):
    form = RegistrationForm()
    if request.method == 'POST':
        username = request.POST.get('uname')
        emailaddress = request.POST.get('emailaddress')
        firstname = request.POST['firstname']
        lastname = request.POST['lastname']
        parentname = request.POST['parentname']
        gender = request.POST['gender']
        mobilenumber = request.POST['mobilenumber']
        emailaddress = request.POST['emailaddress']
        aadharnumber = request.POST['aadharnumber']
        address = request.POST['address']
        city = request.POST['city']
        pincode = request.POST['pincode']
        application_details = create_applicationform(firstname = firstname, lastname = lastname, parentname = parentname, gender = gender, 
                                                     mobilenumber= mobilenumber, emailaddress = emailaddress, aadharnumber = aadharnumber, address  =address, city = city, pincode = pincode,
                                                      )
        application_details.save()
        user = create_applicationform(uname=username,emailaddress = emailaddress)

        domain_name = get_current_site(request).domain
        token = str(random.random()).split('.')[1]
        user.token = token
        link = f'http://{domain_name}/NetBanking/verify/{token}'
        send_mail(
            'Email Verification',
            f'Please click {link} to verify your email',
            settings.EMAIL_HOST_USER,
            [emailaddress],
            fail_silently=False,
        )
        
        return redirect('login')

 

        #http://my-domin.com/verify/<token>
    return render(request,'ApplicationForm.html', {'form':form})
     

def verify(request,token):
    try:
        user = create_applicationform.objects.filter(token = token)
        if user:
           user.verified = True
           msg = 'Your email has been  verified'
           return render(request, 'info.html',{'msg':msg})
    except Exception as e:
        msg = e
        return render(request,'info.html',{'msg': e})

def  ManagerPortal(request):
    applicants = create_applicationform.objects.all()
    context = {'context' : applicants}
    return render(request, "Managerportal.html", context)  

def get_random_password():
    MAX_LEN = 8
    DIGITS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    COMBINED_LIST = DIGITS 
    rand_digit = random.choice(DIGITS)
    temp_pass = rand_digit 
    for x in range(MAX_LEN - 4):
        temp_pass = temp_pass + random.choice(COMBINED_LIST)
        temp_pass_list = array.array('u', temp_pass)
        random.shuffle(temp_pass_list)
        password = ""
    for x in temp_pass_list:
        password = password + x
    return password

def authorize_users(request, id):
    applicant = create_applicationform.objects.get(pk = id)
    create_applicationform.objects.filter(pk=id).delete()
    user_id = applicant.firstname + '_' + applicant.lastname
    password = get_random_password()
    subject = 'Congratulations'
    user = User.objects.create_user(username = user_id, password = password, email = applicant.emailaddress)
    account = account_details(account_holder = user, account_number = account_number())
    account.save()
    message = f'''Hi,
    Your account has been activated
    Username : {user_id}
    password : {password}
    Your account number : {account.account_number}
    Thank you'''
    send_mail( subject, message, settings.EMAIL_HOST_USER, [applicant.emailaddress])
    return HttpResponse('ok')

def loan_amount(request):
    if request.method == 'POST':
        amount = request.POST.get('amount')
        interest = 15
        years = request.POST.get('years')
        loan = Loans(user = request.user, loan_amount = amount, years = years)
        loan.save()
        account = request.user.account_details
        account.balance += int(amount)
        account.save()
        return redirect('check_balance')

def  check_balance(request):
    applicants = account_details.objects.all()
    context = {'context' : applicants}
    return render(request, "check.html", context) 

def transaction(request):
    if request.method == 'GET':
        return render(request,'transaction.html')
    elif request.method == 'POST':
        receiver = request.POST.get('receiver')
        accountnumber = request.POST.get('accountnumber')
        amount = int(request.POST.get('amount'))
        print(receiver)
        print(accountnumber)
        print(amount)
        details = account_details.objects.get(account_number = accountnumber)
        username = details.account_holder.username
        print(details.balance)
        details.balance += amount
        print(details.balance)
        print((account_number))
        details.save()
        intial = account_details.objects.get(account_holder = request.user)
        intialamount = intial.balance
        user = account_details.objects.get(account_holder = request.user)
        print(user.balance)
        user.balance -= amount
        user.save()
        print(user.balance)
        transaction = Transactions(account_holder = request.user, sender_receiver = username, intial = intialamount, final = user.balance, amount = amount)
        transaction.save()
        return redirect('success')
        # context =Transactions.objects.filter(account_holder = request.user)
        # return render(request,'ministatement.html', {'context':context})
    return redirect('failed')

def mini_statement(request):
    context =Transactions.objects.filter(account_holder = request.user)
    return render(request,'ministatement.html', {'context':context})