from django import forms
from .models import create_applicationform

class RegistrationForm(forms.ModelForm):
    class Meta:
        model = create_applicationform
        fields = ('uname','emailaddress')